/root/{{ salt['pillar.get']('org', 'not_found') }}:
  file.managed:
    - source: salt://base/files/template.conf
    - mode: 644
    - owner: root
    - group: root
    - template: jinja
