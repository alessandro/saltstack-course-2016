base:
  '*':
    - base.epel
    - webserver
    - base.sshd
    - base.fail2ban
    - base.bashrc
    - base.files
    - users
